#!/usr/bin/env bash
wget http://central.maven.org/maven2/junit/junit/4.12/junit-4.12.jar
wget http://central.maven.org/maven2/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar
wget http://central.maven.org/maven2/net/objecthunter/exp4j/0.4.8/exp4j-0.4.8.jar
wget https://jitpack.io/com/github/jvm-redux/jvm-redux-api/api/2.0.0/api-2.0.0.jar
wget http://central.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.9.5/jackson-annotations-2.9.5.jar
wget http://central.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.9.5/jackson-core-2.9.5.jar
wget http://central.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.9.5/jackson-databind-2.9.5.jar
