package ija.internal;

import ija.model.Block;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class TestUtils {

    static Set<Block> generate(Integer number, Block... blocks) {
        Set<Block> collect = IntStream.range(1, number + 1)
                .mapToObj(it -> new Block())
                .collect(Collectors.toSet());
        Collections.addAll(collect, blocks);
        return collect;
    }
}
