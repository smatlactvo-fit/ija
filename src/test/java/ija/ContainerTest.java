package ija;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ContainerTest {

    Container container;

    @Before
    public void setUp() throws Exception {
        this.container = new Container();
    }

    @Test
    public void register() {
        Integer a = 5;
        this.container.register(Integer.class, () -> a);
        Assert.assertSame(a, this.container.get(Integer.class));
    }
}