package ija.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

public class SetUtilsTest {

    private Set<String> generate(Integer number) {
        return IntStream.range(1, number + 1)
                .mapToObj(it -> String.format("item%d", it))
                .collect(Collectors.toSet());
    }

    @Test
    public void set() {
        assertTrue(CollectionUtils.set().isEmpty());
    }

    @Test
    public void setCopy() {
        Set<String> items = generate(5);
        Set<String> actual = CollectionUtils.set(items);

        assertEquals(items, actual);
        assertNotSame(items, actual);
    }

    @Test
    public void setOfItems() {
        Set<String> items = generate(3);
        Set<String> actual = CollectionUtils.set("item1", "item2", "item3");

        assertEquals(items, actual);
        assertNotSame(items, actual);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void immutable() {
        Set<String> items = new HashSet<>();
        Set<String> actual = CollectionUtils.immutableSet();
        
        assertEquals(items, actual);
        assertNotSame(items, actual);

        actual.add("throw exception");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void immutableCopy() {
        Set<String> items = generate(5);
        Set<String> actual = CollectionUtils.immutableSet(items);

        assertEquals(items, actual);
        assertNotSame(items, actual);

        actual.add("throw exception");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void immutableOfItems() {
        Set<String> items = generate(3);
        Set<String> actual = CollectionUtils.immutableSet("item1", "item2", "item3");

        assertEquals(items, actual);
        assertNotSame(items, actual);

        actual.add("throw exception");
    }
}
