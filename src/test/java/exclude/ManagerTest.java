package ija.internal;

import ija.model.Block;
import ija.model.Port;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class ManagerTest {

    @Test
    public void blocks() {
        Set<Block> val = TestUtils.generate(2);
        Manager manager = new Manager(val);
        assertEquals(manager.getBlocks(), val);
        assertNotSame(manager.getBlocks(), val);
    }

    @Test
    public void insert() {
        Block block = new Block();
        Manager manager = new Manager(TestUtils.generate(2));
        manager.insert(block);
        assertTrue(manager.getBlocks().stream().anyMatch(it -> it == block));
    }

    @Test
    public void remove() {
        Block block = new Block();
        Manager manager = new Manager(TestUtils.generate(2, block));
        manager.remove(block);
        assertTrue(manager.getBlocks().stream().noneMatch(it -> it == block));
    }

    @Test
    public void portOutput() {
        Block block = new Block();
        Manager manager = new Manager(TestUtils.generate(3, block));

        Port port = manager.port(block, null);

        assertNull(port.getInput());
        assertSame(port.getOutput(), block);

        assertTrue(block.getOutputs().contains(port));
        assertTrue(block.getInputs().isEmpty());
    }

    @Test
    public void portInput() {
        Block block = new Block();
        Manager manager = new Manager(TestUtils.generate(3, block));

        Port port = manager.port(null, block);

        assertNull(port.getOutput());
        assertSame(port.getInput(), block);

        assertTrue(block.getInputs().contains(port));
        assertTrue(block.getOutputs().isEmpty());
    }

    @Test
    public void portTwo() {
        Block first = new Block();
        Block second = new Block();
        Manager manager = new Manager(TestUtils.generate(3, first, second));

        Port port = manager.port(first, second);

        assertSame(port.getOutput(), first);
        assertSame(port.getInput(), second);

        assertTrue(first.getInputs().isEmpty());
        assertTrue(first.getOutputs().contains(port));
        assertTrue(second.getInputs().contains(port));
        assertTrue(second.getOutputs().isEmpty());
    }

    @Test
    public void unportEach() {
        Block block = new Block();
        Manager manager = new Manager(TestUtils.generate(0, block));

        manager.unport(manager.port(null, block));

        assertTrue(block.getInputs().isEmpty());
        assertTrue(block.getOutputs().isEmpty());

        manager.unport(manager.port(block, null));

        assertTrue(block.getInputs().isEmpty());
        assertTrue(block.getOutputs().isEmpty());
    }

    @Test
    public void unportTwo() {
        Block first = new Block();
        Block second = new Block();
        Manager manager = new Manager(TestUtils.generate(0, first, second));

        Port port = manager.port(first, second);
        manager.unport(port);

        assertTrue(first.getInputs().isEmpty());
        assertTrue(first.getOutputs().isEmpty());
        assertTrue(second.getInputs().isEmpty());
        assertTrue(second.getOutputs().isEmpty());
    }

    @Test
    public void noInitial() {
        Manager manager = new Manager(TestUtils.generate(2));
        assertEquals(manager.getInitial().count(), 0);
    }

    @Test
    public void anyInitial() {
        Block block = new Block();
        Manager manager = new Manager(TestUtils.generate(3, block));

        manager.port(null, block);

        AtomicBoolean run = new AtomicBoolean(false);
        manager.getInitial().forEach(it -> {
            run.set(true);
            it.getInputs().forEach(port -> assertNull(port.getOutput()));
            assertEquals(block, it); // should have only one!
        });
        assertTrue(run.get());
    }
}