package ija.calc.exp4j;

import ija.model.DomainEntry;
import net.objecthunter.exp4j.Expression;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Equation implements ija.calc.Equation {
    private Expression compiledExpression;
    private String originalExpression;

    public Equation(Expression compiledExpression, String originalExpression) {
        this.compiledExpression = compiledExpression;
        this.originalExpression = originalExpression;
    }

    @Override
    public List<String> getVariables() {
        return new ArrayList<>(this.compiledExpression.getVariableNames());
    }

    @Override
    public String getExpression() {
        return this.originalExpression;
    }

    @Override
    public Double execute(List<DomainEntry> variables) {
        this.compiledExpression.setVariables(variables.stream().collect(Collectors.toMap(DomainEntry::getName, DomainEntry::getValue)));
        return this.compiledExpression.evaluate();
    }
}
