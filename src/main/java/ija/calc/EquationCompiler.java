package ija.calc;

/**
 * Class representation of compiler for equation.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public interface EquationCompiler {
    /** Compile string expression into equation. */
    Equation compile(String expression);
}
