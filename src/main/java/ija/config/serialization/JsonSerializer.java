package ija.config.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ija.model.Scheme;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Model serializer into json format
 *
 * @author Pavel Parma (xparma02)
 */
public class JsonSerializer implements Serializer {

    private ObjectMapper mapper;

    public JsonSerializer() {
        this.mapper = new ObjectMapper();
        this.mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    @Override
    public String serialize(Collection<Scheme> schemes) {
        try {
            return this.mapper.writeValueAsString(schemes);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Collection<Scheme> deserialize(String serialization) {
        try {
            return Arrays.stream(mapper.readValue(serialization, Scheme[].class)).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
