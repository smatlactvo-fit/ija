package ija.config.serialization;


import ija.model.Scheme;

import java.util.Collection;

/**
 * Serializer interface as dependency inversion principle and adapter or strategy pattern
 *
 * @author Pavel Parma (xparma02)
 */
public interface Serializer {
    String serialize(Collection<Scheme> schemes);
    Collection<Scheme> deserialize(String serialization);
}
