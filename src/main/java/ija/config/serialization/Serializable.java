package ija.config.serialization;

/**
 * Serializable interface mark models designate to serialization
 *
 * @author Pavel Parma (xparma02)
 */
public interface Serializable {
}
