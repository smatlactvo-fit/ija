package ija.utils;

import ija.gui.components.Component;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import java.io.IOException;
import java.util.Objects;
import java.util.Random;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.DragEvent;
import javafx.scene.layout.Pane;

public final class ComponentUtils extends AbstractUtils {
    private static ClassLoader classLoader = FXMLLoader.getDefaultClassLoader();
    private static Random random = new Random();

    public static <T> T load(String fxml, Component controller) {
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(classLoader.getResource(fxml)));
        fxmlLoader.setController(controller);

        try {
            T view = Objects.requireNonNull(fxmlLoader.load());

            if (view instanceof Node)
                ((Node) view).setUserData(controller);

            Platform.runLater(controller::onInit);

            return view;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Point2D center(Node node) {
        Bounds bounds = node.getLayoutBounds();
        return pos(node).add(bounds.getWidth() / 2, bounds.getHeight() / 2);
    }


    public static Point2D midpoint(Node first, Node second) {
        return pos(first).midpoint(pos(second));
    }

    public static Point2D random(Node node, Bounds bounds) {
        Bounds nodeBounds = node.getLayoutBounds();

        Double x = Math.random() * (bounds.getWidth() - nodeBounds.getWidth());
        Double y = Math.random() * (bounds.getHeight() - nodeBounds.getHeight());

        return pos(node).add(x,y);
    }

    public static Point2D pos(Node node) {
        return node.localToScene(0, 0);
    }

    public static Point2D pos(Point2D local, Node context) {
        return context.localToScene(local);
    }

    private static Point2D parent(Node node) {
        return node.localToParent(0, 0);
    }

    private static Point2D parentOfScene(Point2D scene, Node context) {
        Point2D dim = scene.subtract(pos(context));
        return parent(context).add(dim);
    }

    public static void translate(Node node, Point2D scene) {
        Point2D position = parentOfScene(scene, node);
        node.relocate(position.getX(), position.getY());
    }

    public static void autoremove(Node node) {
        Parent parent = node.getParent();
        if (parent instanceof Pane) ((Pane) parent).getChildren().remove(node);
        else if (parent instanceof Group) ((Group) parent).getChildren().remove(node);
    }

    public static boolean noLinkBetween(BlockComp block, PortComp port) {
        return CollectionUtils.list(port.getSource(), port.getTarget()).stream()
                .filter(Objects::nonNull)
                .allMatch(it -> it.getBlock() != block);
    }

    public static boolean canBeLinked(PortComp port) {
        return CollectionUtils.list(port.getSource(), port.getTarget()).stream()
                .anyMatch(Objects::nonNull);
    }

    @SuppressWarnings("unchecked")
    public static <T> T comp(DragEvent event) {
        return (T)((Node)event.getGestureSource()).getUserData();
    }

}
