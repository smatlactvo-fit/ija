package ija;

import ija.calc.EquationCompiler;
import ija.config.serialization.JsonSerializer;
import ija.config.storage.FileStorage;
import ija.config.storage.Storage;
import ija.gui.components.app.App;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.nio.file.Paths;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        initContainer();
        App app = new App();
        primaryStage.setScene(new Scene(app.getView()));
        primaryStage.show();


    }

    private void initContainer() {
        Container container = Container.getInstance();
        container.register(EquationCompiler.class, ija.calc.exp4j.EquationCompiler::new);
        container.register(Storage.class, () -> new FileStorage(Paths.get("ija.json"), new JsonSerializer()));
    }
}
