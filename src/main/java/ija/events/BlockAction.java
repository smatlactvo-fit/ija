package ija.events;

import ija.calc.Equation;
import ija.model.Block;

public interface BlockAction extends ContentAction {
    Block getBlock();

    class ChangeEquation implements BlockAction {
        public final Block block;
        public final Equation equation;

        public ChangeEquation(Block block, Equation equation) {
            this.block = block;
            this.equation = equation;
        }

        @Override
        public Block getBlock() {
            return block;
        }
    }

    class ComputeAction implements BlockAction {
        public final Block block;
        public final String result;

        public ComputeAction(Block block, String result) {
            this.block = block;
            this.result = result;
        }

        @Override
        public Block getBlock() {
            return block;
        }
    }
}
