package ija.events;

import ija.model.Block;
import ija.model.Port;
import javafx.geometry.Point2D;

public interface ContentAction extends Action {

    class AddBlock implements ContentAction {
        public final Point2D position;
        public final Block block;

        public AddBlock(Point2D position, Block block) {
            this.position = position;
            this.block = block;
        }
    }

    class RemoveBlock implements ContentAction {
        public final Block block;

        public RemoveBlock(Block block) {
            this.block = block;
        }
    }

    class AddPort implements ContentAction {
        public final Point2D position;
        public final Port port;

        public AddPort(Point2D position, Port port) {
            this.position = position;
            this.port = port;
        }
    }

    class RemovePort implements ContentAction {
        public final Port port;

        public RemovePort(Port port) {
            this.port = port;
        }
    }
}
