package ija.gui;

import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class MenuItemBuilder {

    private String text;
    private Runnable runnable;

    public MenuItemBuilder text(String text) {
        this.text = text;
        return this;
    }

    public MenuItemBuilder action(Runnable runnable) {
        this.runnable = runnable;
        return this;
    }

    public MenuItem build() {
        MenuItem item = new MenuItem(text);
        item.setOnAction(it -> {
            runnable.run();
            it.consume();
        });

        return item;
    }

    public static MenuItemBuilder of(String text) {
        return new MenuItemBuilder().text(text);
    }

    public static MenuItem separator() {
        return new SeparatorMenuItem();
    }
}
