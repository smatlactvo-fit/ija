package ija.gui;

import ija.calc.EquationCompiler;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import ija.model.Block;
import ija.model.Port;
import ija.model.Scheme;
import ija.utils.CollectionUtils;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class State {
    private List<Scheme> schemes;
    private ActiveScheme activeScheme;

    public State() {
        this(CollectionUtils.list());
    }

    public State(List<Scheme> schemes) {
        this.schemes = schemes;
    }

    public List<Scheme> getSchemes() {
        return schemes;
    }

    public Scheme getScheme() {
        if (getActiveScheme() == null) throw new IllegalStateException("No active scheme!");
        else return schemes.get(getActiveScheme().getIndex());
    }

    public ActiveScheme getActiveScheme() {
        return activeScheme;
    }

    public void setActiveScheme(ActiveScheme activeScheme) {
        this.activeScheme = activeScheme;
    }

    public static class ActiveScheme {
        private final int index;
        private final Map<Block, BlockComp> blocks = CollectionUtils.map();
        private final Map<Port, PortComp> ports = CollectionUtils.map();

        public ActiveScheme(int index) {
            this.index = index;
        }

        public Map<Block, BlockComp> getBlocks() {
            return blocks;
        }

        public Map<Port, PortComp> getPorts() {
            return ports;
        }

        public int getIndex() {
            return index;
        }

        public Set<Block> getInitial() {
            return ports.keySet().stream()
                    .filter(it -> it.getSource() == null)
                    .map(Port::getTarget)
                    .collect(Collectors.toSet());
        }
    }
}
