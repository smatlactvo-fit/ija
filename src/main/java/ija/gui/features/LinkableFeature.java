package ija.gui.features;

import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;

public class LinkableFeature extends AbstractFeature {

    private Predicate<DragEvent> when;
    private Node applicable;
    private Consumer<DragEvent> link;

    //region Setup
    public LinkableFeature when(Predicate<DragEvent> when) {
        check();
        this.when = when;
        return this;
    }

    public LinkableFeature from(Predicate<Object> from) {
        check();
        this.when = it -> from.test(it.getGestureSource());
        return this;
    }

    public LinkableFeature applyTo(Node applicable) {
        check();
        this.applicable = applicable;
        return this;
    }

    public LinkableFeature link(Consumer<DragEvent> link) {
        check();
        this.link = link;
        return this;
    }
    //endregion

    //region Handlers
    private final EventHandler<DragEvent> dragOver = it -> {
        if (when != null && !when.test(it)) return;

        it.acceptTransferModes(TransferMode.LINK);

        it.consume();
    };

    private final EventHandler<DragEvent> dragDropped = it -> {
        if (it.isAccepted() && it.getTransferMode() == TransferMode.LINK) {
            if (link != null) link.accept(it);
            it.setDropCompleted(true);

            it.consume();
        }
    };
    //endregion

    @Override
    protected void validateAndLock() {
        super.validateAndLock();

        Objects.requireNonNull(applicable);
    }

    @Override
    public Featured install() {
        validateAndLock();

        applicable.addEventHandler(DragEvent.DRAG_OVER, dragOver);
        applicable.addEventHandler(DragEvent.DRAG_DROPPED, dragDropped);

        return this::uninstall;
    }

    private void uninstall() {
        applicable.removeEventHandler(DragEvent.DRAG_OVER, dragOver);
        applicable.removeEventHandler(DragEvent.DRAG_DROPPED, dragDropped);
    }

    public static LinkableFeature create() {
        return new LinkableFeature();
    }
}
