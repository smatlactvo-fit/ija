package ija.gui.features;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.transform.Translate;
import javax.xml.crypto.Data;

public class PlatformMoveFeature extends AbstractFeature {

    private static final DataFormat DEFAULT = new DataFormat("#move-feature");

    private BooleanProperty me = new SimpleBooleanProperty();

    private SimpleObjectProperty<Point2D> original = new SimpleObjectProperty<>();
    private SimpleObjectProperty<Point2D> vector = new SimpleObjectProperty<>();

    private Translate translate = new Translate();

    private Predicate<MouseEvent> when;
    private Node applicable;
    private Set<Node> focusedOn = new HashSet<>();

    private Map<DataFormat, Object> data = new ClipboardContent();

    //region Setup
    public PlatformMoveFeature when(Predicate<MouseEvent> when) {
        check();
        this.when = when;
        return this;
    }

    public PlatformMoveFeature applyTo(Node node) {
        check();
        this.applicable = node;
        return this;
    }

    public PlatformMoveFeature observeOn(Node... node) {
        check();
        Collections.addAll(focusedOn, node);
        return this;
    }

    public PlatformMoveFeature with(DataFormat key, Serializable value) {
        check();
        data.put(key, value);
        return this;
    }
    //endregion

    //region Handlers
    private final EventHandler<MouseEvent> dragDetected = it -> {
        if (!when.test(it)) return;

        me.set(true);

        original.set(translate.transform(0, 0));
        Point2D cursor = new Point2D(it.getSceneX(), it.getSceneY());
        vector.set(cursor.subtract(original.get()));

        if (data.isEmpty()) data.put(DEFAULT, UUID.randomUUID());
        applicable.startDragAndDrop(TransferMode.MOVE).setContent(data);

        it.consume();
    };

    private final EventHandler<DragEvent> dragDone = it -> {
        if (!me.get()) return;

        me.set(false);

        // TODO: when is not done!
        if (!it.isAccepted()) {
            Point2D original1 = this.original.get();
            applicable.relocate(original1.getX(), original1.getY());
        }

        it.consume();
    };

    private final EventHandler<DragEvent> dragOver = it -> {
        if (!me.get()) return;

        Point2D cursor = new Point2D(it.getSceneX(), it.getSceneY());
        Point2D res = cursor.subtract(vector.get());

        translate.setX(res.getX());
        translate.setY(res.getY());

        it.acceptTransferModes(TransferMode.MOVE);

        it.consume();
    };

    private final EventHandler<DragEvent> dragDropped = it -> {
        if (!me.get()) return;

        it.setDropCompleted(true);

        it.consume();
    };

    //endregion

    @Override
    protected void validateAndLock() {
        super.validateAndLock();

        Objects.requireNonNull(when);
        Objects.requireNonNull(applicable);
    }

    @Override
    public Featured install() {
        validateAndLock();

        applicable.getTransforms().add(translate);
        applicable.mouseTransparentProperty().bind(me);

        applicable.addEventHandler(MouseEvent.DRAG_DETECTED, dragDetected);
        applicable.addEventHandler(DragEvent.DRAG_DONE, dragDone);

        focusedOn.forEach(focusedOn -> {
            focusedOn.addEventHandler(DragEvent.DRAG_OVER, dragOver);
            focusedOn.addEventHandler(DragEvent.DRAG_DROPPED, dragDropped);
        });

        return this::uninstall;
    }

    private void uninstall() {
        applicable.getTransforms().remove(translate);
        applicable.mouseTransparentProperty().unbind();

        applicable.removeEventHandler(MouseEvent.DRAG_DETECTED, dragDetected);
        applicable.removeEventHandler(DragEvent.DRAG_DONE, dragDone);

        focusedOn.forEach(focusedOn -> {
            focusedOn.removeEventHandler(DragEvent.DRAG_OVER, dragOver);
            focusedOn.removeEventHandler(DragEvent.DRAG_DROPPED, dragDropped);
        });
    }

    public static PlatformMoveFeature create() {
        return new PlatformMoveFeature();
    }
}
