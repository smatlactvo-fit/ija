package ija.gui.features;

import ija.gui.MenuItemBuilder;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ContextMenuEvent;

public class ContextMenuFeature extends AbstractFeature {

    private ContextMenu menu = new ContextMenu();

    private Predicate<ContextMenuEvent> when;
    private Node applicable;

    //region Setup
    public ContextMenuFeature when(Predicate<ContextMenuEvent> when) {
        check();
        this.when = when;
        return this;
    }

    public ContextMenuFeature applyTo(Node node) {
        check();
        this.applicable = node;
        return this;
    }

    public BlockItemCreator with(String name) {
        check();
        return new BlockItemCreator().text(name);
    }
    //endregion

    // region Handlers
    private final EventHandler<ContextMenuEvent> contextMenuRequest = it -> {
        if (when != null && !when.test(it)) return;

        menu.show(applicable, it.getScreenX(), it.getScreenY());

        it.consume();
    };
    //endregion

    @Override
    protected void validateAndLock() {
        super.validateAndLock();

        Objects.requireNonNull(applicable);
    }

    @Override
    public Featured install() {
        validateAndLock();

        applicable.addEventHandler(ContextMenuEvent.CONTEXT_MENU_REQUESTED, contextMenuRequest);

        return this::uninstall;
    }

    private void uninstall() {
        applicable.removeEventHandler(ContextMenuEvent.CONTEXT_MENU_REQUESTED, contextMenuRequest);
    }

    public static ContextMenuFeature create() {
        return new ContextMenuFeature();
    }

    // TODO: uninstall
    public class BlockItemCreator {

        private String text;
        private Consumer<ActionEvent> action;

        private final EventHandler<ActionEvent> actionHandler = it -> {
            action.accept(it);

            it.consume();
        };

        public BlockItemCreator text(String text) {
            this.text = text;
            return this;
        }

        public BlockItemCreator execute(Consumer<ActionEvent> action) {
            this.action = action;
            return this;
        }

        public ContextMenuFeature include() {
            Objects.requireNonNull(text);

            MenuItem menuItem = new MenuItem();
            menuItem.setText(text);
            if (action != null) menuItem.addEventHandler(ActionEvent.ACTION, actionHandler);

            menu.getItems().add(menuItem);
            return ContextMenuFeature.this;
        }

    }
}
