package ija.gui.features;

import ija.utils.CollectionUtils;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.Predicate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.transform.Translate;

public class MoveFeature extends AbstractFeature {
    private BooleanProperty me = new SimpleBooleanProperty();

    /** Vector from cursor to already transformed point */
    private SimpleObjectProperty<Point2D> vector = new SimpleObjectProperty<>();
    private Translate translate = new Translate();

    private Predicate<MouseEvent> when;
    private Node applicable;
    private HashSet<Node> focusedOn = new HashSet<>();

    //region Setup
    public MoveFeature when(Predicate<MouseEvent> filter) {
        check();
        this.when = filter;
        return this;
    }

    public MoveFeature applyTo(Node node) {
        check();
        this.applicable = node;
        return this;
    }

    public MoveFeature observeOn(Node... node) {
        check();
        Collections.addAll(focusedOn, node);
        return this;
    }
    //endregion

    //region Handlers
    private final EventHandler<MouseEvent> mousePressed = it -> {
        if (when != null && !when.test(it)) return;

        me.set(true);

        Point2D original = translate.transform(0, 0);
        Point2D cursor = new Point2D(it.getSceneX(), it.getSceneY());
        vector.set(cursor.subtract(original));

        it.consume();
    };

    private final EventHandler<MouseEvent> mouseDragged = it -> {
        if (!me.get()) return;

        Point2D pin = new Point2D(it.getSceneX(), it.getSceneY());
        Point2D res = pin.subtract(vector.get());

        translate.setX(res.getX());
        translate.setY(res.getY());

        it.consume();
    };

    private final EventHandler<MouseEvent> mouseReleased = it -> {
        if (!me.get()) return;

        me.set(false);

        it.consume();
    };

    //endregion

    @Override
    protected void validateAndLock() {
        super.validateAndLock();

        Objects.requireNonNull(applicable);
        validate(!focusedOn.isEmpty());
    }

    @Override
    public Featured install() {
        validateAndLock();

        applicable.getTransforms().add(translate);

        focusedOn.forEach(focusedOn -> {
            focusedOn.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressed);
            focusedOn.addEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDragged);
            focusedOn.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleased);
        });

        return this::uninstall;
    }

    private void uninstall() {
        applicable.getTransforms().remove(translate);

        focusedOn.forEach(focusedOn -> {
            focusedOn.removeEventHandler(MouseEvent.MOUSE_PRESSED, mousePressed);
            focusedOn.removeEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDragged);
            focusedOn.removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleased);
        });
    }

    public static MoveFeature create() {
        return new MoveFeature();
    }
}
