package ija.gui.components;

import ija.gui.features.Feature;
import ija.gui.features.Featured;
import ija.utils.CollectionUtils;
import ija.utils.ComponentUtils;
import java.util.List;
import java.util.UUID;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;

public abstract class ModelComponent<Model, View extends Node> extends AbstractComponent<View> {

    protected final Model model;

    public ModelComponent(Model model, String fmxl) {
        super(fmxl);
        this.model = model;
    }

    public Model getModel() {
        return model;
    }

    //region Feature trait
    private List<Featured> features = CollectionUtils.list();

    public void install(Feature feature) {
        features.add(feature.install());
    }
}
