package ija.gui.components.extra;

import ija.events.LinkAction.RemoveLink;
import ija.gui.State;
import ija.gui.components.AbstractComponent;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import java.util.Optional;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.shape.Line;
import redux.api.Store;

public abstract class Link extends AbstractComponent<Line> {

    private static final String FXML = "fxml/extra/link.fxml";

    private final Store<State> store;

    private BlockComp block;
    private PortComp port;

    public Link(Store<State> store, BlockComp block, PortComp port) {
        super(FXML);
        this.store = store;
        this.block = block;
        this.port = port;
    }

    @Override
    public void onInit() {
        super.onInit();

        view.getStrokeDashArray().addAll(10.0, 5.0);

        initAnchors();
        linkTogether();
    }

    protected abstract void linkTogether();

    private void initAnchors() {
        Node block = this.block.getView();
        this.view.startYProperty().bind(Link.createYAxis(block));
        this.view.startXProperty().bind(Link.createXAxis(block));

        Node port = getPort().getView();
        this.view.endXProperty().bind(Link.createXAxis(port));
        this.view.endYProperty().bind(Link.createYAxis(port));
    }

    public void remove() {
        // from Port - links are not null
        // from Block - links are null
        Optional.ofNullable(block.getLinks()).ifPresent(it -> it.remove(this));

        store.dispatch(new RemoveLink(block, port, this));
    }

    public BlockComp getBlock() {
        return block;
    }

    public PortComp getPort() {
        return port;
    }

    private static DoubleBinding createXAxis(Node observable) {
        return Bindings.createDoubleBinding(() -> {
            Bounds bounds = observable.getBoundsInParent();
            return bounds.getMinX() + bounds.getWidth() / 2;
        }, observable.boundsInParentProperty());
    }

    private static DoubleBinding createYAxis(Node observable) {
        return Bindings.createDoubleBinding(() -> {
            Bounds bounds = observable.getBoundsInParent();
            return bounds.getMinY() + bounds.getHeight() / 2;
        }, observable.boundsInParentProperty());
    }
}
