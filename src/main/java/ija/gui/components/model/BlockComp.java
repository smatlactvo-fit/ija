package ija.gui.components.model;

import ija.Container;
import ija.calc.Equation;
import ija.calc.EquationCompiler;
import ija.events.Action;
import ija.events.BlockAction.ChangeEquation;
import ija.events.BlockAction.ComputeAction;
import ija.events.ContentAction.RemoveBlock;
import ija.gui.MenuItemBuilder;
import ija.gui.State;
import ija.gui.components.ModelComponent;
import ija.gui.components.extra.Link;
import ija.model.Block;
import ija.utils.CollectionUtils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import redux.api.Store;

public class BlockComp extends ModelComponent<Block, VBox> {
    private static final String FXML_VIEW = "fxml/model/block.fxml";

    public static final DataFormat LINKING = new DataFormat("linking-from-target");
    public static final DataFormat MOVING = new DataFormat("target-moving");

    private final Store<State> store;

    private List<Link> links = CollectionUtils.list();

    @FXML
    private Circle circle;
    @FXML
    private Label equation;
    @FXML
    private Label result;

    private ContextMenu menu = new ContextMenu();
    private Tooltip tooltip = new Tooltip();

    public BlockComp(Store<State> store, Block block) {
        super(block, FXML_VIEW);
        this.store = store;
    }

    @Override
    public void onInit() {
        super.onInit();

        Tooltip.install(getView(), tooltip);

        menu.getItems().addAll(
                MenuItemBuilder.of("Upravit").action(this::edit).build(),
                MenuItemBuilder.of("Odstranit").action(this::remove).build()
        );
    }

    public List<Link> getLinks() {
        return links;
    }

    public void link(Link component) {
        links.add(component);
    }

    private void edit() {
        TextInputDialog dialog = new TextInputDialog(Optional.ofNullable(this.model.getEquation()).map(Equation::getExpression).orElse(null));
        dialog.setGraphic(null);
        dialog.setTitle("Úprava bloku");
        dialog.setHeaderText(null);
        dialog.setContentText("Rovnice:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(s -> {
            Equation equation = Container.getInstance().get(EquationCompiler.class).compile(s);
            store.dispatch(new ChangeEquation(this.model, equation));
        });
    }

    public void remove() {
        Stream<Link> stream = links.stream();
        links = null;
        stream.forEach(link -> {
            link.remove();
            Optional.of(link.getPort())
                    .filter(it -> it.getSource() == null && it.getTarget() == null)
                    .ifPresent(PortComp::remove);
        });

        store.dispatch(new RemoveBlock(this.model));
    }

    @FXML
    private void onMenuRequested(ContextMenuEvent it) {
        menu.show(getView(), it.getScreenX(), it.getScreenY());
        it.consume();
    }

    public void consume(State state, Action action) {
        if (action instanceof ChangeEquation) changeEquation((ChangeEquation) action);
        else if (action instanceof ComputeAction) computeAction((ComputeAction) action);
    }

    private void changeEquation(ChangeEquation action) {
        action.block.setEquation(action.equation);
        equation.setText(action.equation.getExpression());
    }

    private void computeAction(ComputeAction action) {
        result.setText(action.result);
    }
}
