package ija;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

public class Container {
    private Map<Class, Object> instances = new HashMap<>();
    private Map<Class, Supplier> factories = new HashMap<>();
    private static Container instance;

    public <T> void register(Class<T> type, Supplier<T> factory) {
        this.factories.put(type, factory);
    }

    public <T> T get(Class<T> type) {
        if(! instances.containsKey(type)){
            if(! factories.containsKey(type)) throw new RuntimeException(String.format("Type %s is not set in container.", type));
            instances.put(type, factories.get(type).get());
        }

        return (T) instances.get(type);
    }

    public static Container getInstance() {
        if(Objects.isNull(Container.instance)) {
            Container.instance = new Container();
        }

        return Container.instance;
    }
}
