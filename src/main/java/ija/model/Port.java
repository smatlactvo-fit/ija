package ija.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ija.utils.CollectionUtils;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Class representation as relationship between two blocks.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public class Port {
    private List<DomainEntry> type = CollectionUtils.list();
    private Block source;
    private Block target;

    public Port() {}

    public Port(Block source, Block target) {
        this.target = target;
        this.source = source;
    }

    /** Gets type of port. */
    public List<DomainEntry> getType() {
        return CollectionUtils.immutableList(type);
    }

    /** Sets type of port. */
    public void setType(List<DomainEntry> type) {
        this.type = CollectionUtils.list(type);
    }

    @JsonIgnore
    public Map<String, Double> toMap() {
        return type.stream()
                .collect(Collectors.toMap(DomainEntry::getName, DomainEntry::getValue));
    }

    public void fromMap(Map<String, Double> type) {
        this.type = type.entrySet().stream()
                .map(it -> new DomainEntry(it.getKey(), it.getValue()))
                .collect(Collectors.toList());
    }

    /** Gets source target. Can be null. */
    @JsonIgnore
    public Block getSource() {
        return source;
    }

    /** Gets target target. Can be null. */
    @JsonIgnore
    public Block getTarget() {
        return target;
    }

    public void setSource(Block source) {
        this.source = source;
    }

    public void setTarget(Block target) {
        this.target = target;
    }
}
