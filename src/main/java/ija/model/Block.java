package ija.model;


import ija.calc.Equation;
import ija.utils.CollectionUtils;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class representation for equation.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public class Block {
    private List<Port> inputs = CollectionUtils.list();
    private List<Port> outputs = CollectionUtils.list();

    private Equation equation;

    /** Grabs inputs, pass it to the equation and puts it into outputs. */
    public void execute() {
        // TODO: check logic
        final List<Double> values = inputs.stream()
                .map(port -> equation.execute(port.getType()))
                .map(Objects::requireNonNull)
                .collect(Collectors.toList());

        outputs.forEach(port -> {
            List<String> names = port.getSource().getEquation().getVariables();

            if (names.size() != values.size()) throw new IllegalStateException("names.size() != values.size()");

            List<DomainEntry> list = CollectionUtils.list();
            for(int i = 0; i < names.size(); ++i)
                list.add(new DomainEntry(names.get(i), values.get(i)));
            port.setType(list);
        });
    }

    /** Gets equation. */
    public Equation getEquation() {
        return equation;
    }

    /** Sets equation. */
    public void setEquation(Equation equation) {
        this.equation = equation;
    }

    /** Gets inputs. */
    public List<Port> getInputs() {
        return CollectionUtils.immutableList(inputs);
    }

    /** Sets inputs. */
    public void setInputs(List<Port> inputs) {
        this.inputs = CollectionUtils.list(inputs);
    }

    /** Gets outputs. */
    public List<Port> getOutputs() {
        return CollectionUtils.immutableList(outputs);
    }

    /** Sets outputs. */
    public void setOutputs(List<Port> outputs) {
        this.outputs = CollectionUtils.list(outputs);
    }
}
